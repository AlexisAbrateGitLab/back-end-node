var express = require('express');
const apiRouter = express.Router();
require('dotenv').config()
var myGenericMongoClient = require('./my_generic_mongo_client');

var myMongoDbUrl = process.env.URL_DB
//console.log("myMongoDbUrl="+myMongoDbUrl)
myGenericMongoClient.setMongoDbUrl(myMongoDbUrl);




apiRouter.route('/classement/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    //console.log(season);
    var league = Number(req.params.league);
    //console.log(league);

    myGenericMongoClient.genericFindOne('standings',
    {'league.season' : season , 'league.id' : league},
    function(err,standings){
            res.send(standings);
    })
}

);

/*apiRouter.route('/classement/topscorer/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    var topList = []
   var butE = []
    myGenericMongoClient.genericFindList('top_scorer',
    {'statistics.league.season' : season , 'statistics.league.id' : league},
    function(err,top){
            
            for(i=0;i<10;i++) {
                dataJson = {"team_id": 0,
                            "player_id" : 0,
                            "logo":"",
                            "name": "",
                            "goals" : 0,
                            "but_team": 0,
                            "photo_player":""
                        }
                dataJson.team_id = top[i].statistics[0].team.id
                dataJson.player_id = top[i].player.id
                dataJson.logo = top[i].statistics[0].team.logo
                dataJson.name = top[i].player.name
                dataJson.goals = top[i].statistics[0].goals.total
                dataJson.photo_player = top[i].player.photo

                myGenericMongoClient.genericFindOne('stats',
                {'team.id' : dataJson.team_id, 'league.season' : season} , function(err,team) {
                    console.log(team.goals.for.total.total)
                     butE.push(team.goals.for.total.total)
                    
                })

                console.log(butE)
                topList.push(dataJson)
            }

            
            res.send(topList);
    })
}

);
*/

apiRouter.route('/classement/topscorer/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    //console.log(season);
    var league = Number(req.params.league);
    //console.log(league);

    myGenericMongoClient.genericFindList('top_scorer',
    {'statistics.league.season' : season , 'statistics.league.id' : league},
    function(err,top){
            var topList = []
            for(i=0;i<10;i++) {
                topList[i] = top[i]
            }
           // console.log(topList);
            res.send(topList);
    })
}

)

apiRouter.route('/classement/topassist/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    myGenericMongoClient.genericFindList('top_assist',
    {'statistics.league.season' : season , 'statistics.league.id' : league},
    function(err,top){
            var topList = []
            for(i=0;i<10;i++) {
                topList[i] = top[i]
            }
            //console.log(topList);
            res.send(topList);
    })
}

);

apiRouter.route('/classement/notes/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    myGenericMongoClient.genericFindList('players',
    {'statistics.league.season' : season , 'statistics.league.id' : league},
    function(err,top){

        top.sort(function (a,b) {

            var a1st = -1; //negative value means left item should appear first
            var b1st =  1; //positive value means right item should appear first
            var equal = 0; //zero means objects are equal
        
            //compare your object's property values and determine their order
            if (a.statistics[0].games.rating < b.statistics[0].games.rating) {
                return b1st;
            }
            else if (b.statistics[0].games.rating < a.statistics[0].games.rating) {
                return a1st;
            }
            else {
                return equal;
            }
           })

           var listWithApperence = []
           top.forEach(element => {
               if(element.statistics[0].games.appearences > 10) {
                   listWithApperence.push(element)
               }
           })
           

           var topList = []
           for(i=0;i<10;i++) {
               var note = Number(listWithApperence[i].statistics[0].games.rating)
               var noteround = note.toFixed(2)
               listWithApperence[i].statistics[0].games.rating = noteround
               topList[i] = listWithApperence[i]
           }

           res.send(topList);
           
    })
}

);


apiRouter.route('/classement/stats/goalchart/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);
    var chartsData = []

    myGenericMongoClient.genericFindList('stats',
     {'league.season' : season, 'league.id' : league},
     function(err,dataFor) {
         chartsData.push(dataFor)
     })

     myGenericMongoClient.genericFindList('stats',
     {'league.season' : season, 'league.id' : league},
     function(err,dataAg) {
         chartsData.push(dataAg)
         res.send(chartsData)
     })

     

})


apiRouter.route('/classement/stats/goalsfor/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    myGenericMongoClient.genericFindList('stats',
    {'league.season' : season , 'league.id' : league},
    function(err,standings){
            //console.log(standings);

            standings.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (a.goals.for.total.total < b.goals.for.total.total) {
                    return b1st;
                }
                else if (b.goals.for.total.total < a.goals.for.total.total) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

            res.send(standings);
    })
}

);


apiRouter.route('/classement/stats/goalsagainst/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    myGenericMongoClient.genericFindList('stats',
    {'league.season' : season , 'league.id' : league},
    function(err,standings){
            //console.log(standings);

            standings.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (a.goals.against.total.total < b.goals.against.total.total) {
                    return b1st;
                }
                else if (b.goals.against.total.total < a.goals.against.total.total) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

            res.send(standings);
    })
}

);

apiRouter.route('/classement/stats/strikewin/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    myGenericMongoClient.genericFindList('stats',
    {'league.season' : season , 'league.id' : league},
    function(err,standings){
            //console.log(standings);

            standings.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (a.biggest.streak.wins < b.biggest.streak.wins) {
                    return b1st;
                }
                else if (b.biggest.streak.wins < a.biggest.streak.wins) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

            res.send(standings);
    })
}

);

apiRouter.route('/classement/stats/strikelose/:season/:league')
.get( function(req,res,next) {
    var season = Number(req.params.season);
    console.log(season);
    var league = Number(req.params.league);
    console.log(league);

    myGenericMongoClient.genericFindList('stats',
    {'league.season' : season , 'league.id' : league},
    function(err,standings){
            //console.log(standings);

            standings.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (a.biggest.streak.lose < b.biggest.streak.lose) {
                    return b1st;
                }
                else if (b.biggest.streak.lose < a.biggest.streak.lose) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

            res.send(standings);
    })
}

);


apiRouter.route('/classement/BestXI/Attacker/:id/:season')
.get( function(req,res,next) {
    
    var id = Number(req.params.id)
    var season = Number(req.params.season)

    myGenericMongoClient.genericFindList('players',
    {'statistics.league.id':id,'statistics.league.season':season,'statistics.games.position':"Attacker","statistics.games.appearences": {$gt: 10}}, 
        function(err,players) {
            players.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (b.statistics[0].games.rating > a.statistics[0].games.rating) {
                    return b1st;
                }
                else if (a.statistics[0].games.rating > b.statistics[0].games.rating) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

               attacker = []
               attacker.push(players[0], players[1])
               for(let attack of attacker ) {
                var note = Number(attack.statistics[0].games.rating)
                var noteround = note.toFixed(2)
                //console.log(noteround)
                attack.statistics[0].games.rating = noteround
                
            }
               res.send(attacker)

    })
})


apiRouter.route('/classement/BestXI/Defender/:id/:season')
.get( function(req,res,next) {
    
    var id = Number(req.params.id)
    var season = Number(req.params.season)

    myGenericMongoClient.genericFindList('players',
    {'statistics.league.id':id,'statistics.league.season':season,'statistics.games.position':"Defender","statistics.games.appearences": {$gt: 10}}, 
        function(err,players) {
            players.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (b.statistics[0].games.rating > a.statistics[0].games.rating) {
                    return b1st;
                }
                else if (a.statistics[0].games.rating > b.statistics[0].games.rating) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

               defender = []
               defender.push(players[0], players[1],players[2],players[4])
               for(let defend of defender ) {
                var note = Number(defend.statistics[0].games.rating)
                var noteround = note.toFixed(2)
               // console.log(noteround)
                defend.statistics[0].games.rating = noteround
                
            }
               res.send(defender)

    })
})

apiRouter.route('/classement/BestXI/Midfielder/:id/:season')
.get( function(req,res,next) {
    
    var id = Number(req.params.id)
    var season = Number(req.params.season)

    myGenericMongoClient.genericFindList('players',
    {'statistics.league.id':id,'statistics.league.season':season,'statistics.games.position':"Midfielder","statistics.games.appearences": {$gt: 10}}, 
        function(err,players) {
            players.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (b.statistics[0].games.rating > a.statistics[0].games.rating) {
                    return b1st;
                }
                else if (a.statistics[0].games.rating > b.statistics[0].games.rating) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

               midfielder = []
               midfielder.push(players[0], players[1],players[2],players[4])
               for(let mid of midfielder ) {
                var note = Number(mid.statistics[0].games.rating)
                var noteround = note.toFixed(2)
               // console.log(noteround)
                mid.statistics[0].games.rating = noteround
                
            }
               res.send(midfielder)

    })
})



apiRouter.route('/classement/BestXI/Goalkeeper/:id/:season')
.get( function(req,res,next) {
    
    var id = Number(req.params.id)
    var season = Number(req.params.season)

    myGenericMongoClient.genericFindList('players',
    {'statistics.league.id':id,'statistics.league.season':season,'statistics.games.position':"Goalkeeper","statistics.games.appearences": {$gt: 10}}, 
        function(err,players) {
            players.sort(function (a,b) {

                var a1st = -1; //negative value means left item should appear first
                var b1st =  1; //positive value means right item should appear first
                var equal = 0; //zero means objects are equal
            
                //compare your object's property values and determine their order
                if (b.statistics[0].games.rating > a.statistics[0].games.rating) {
                    return b1st;
                }
                else if (a.statistics[0].games.rating > b.statistics[0].games.rating) {
                    return a1st;
                }
                else {
                    return equal;
                }
               })

               goalkeeper = []
               goalkeeper.push(players[0])
               var note = Number(goalkeeper[0].statistics[0].games.rating)
               var notefixed = note.toFixed(2)
               goalkeeper[0].statistics[0].games.rating = notefixed
               res.send(goalkeeper)

    })
})

exports.apiRouter = apiRouter;



/*apiRouter.route('/classement/:season/:league')
.get( function(req,res,next) {
    var season = req.params.season;
    console.log(season);
    var league = req.params.league;
    console.log(league);

    myGenericMongoClient.genericFindList('standings',
    {'league.season' : season , 'league.id' : league},
    function(err,standing){
        if(standing==null)my
            res.status(404).send({ err : 'mynoleague found'})
        elsemy
            res.send(standing);
    })
}

)*/