var express = require('express');
const apiRouter = express.Router();
require('dotenv').config()
var myGenericMongoClient = require('./my_generic_mongo_client');

var myMongoDbUrl = process.env.URL_DB
myGenericMongoClient.setMongoDbUrl(myMongoDbUrl);


/////////////////////////// Recherche matchs championnat par Journée ////////////////////////////////////

apiRouter.route('/fixtures/season/champ/journee/:season/:id_champ/:journee')
.get( function(req,res,next) {

    var id_champ = Number(req.params.id_champ)
    var journee = Number(req.params.journee)
    var season = Number(req.params.season)
    console.log(id_champ + " et " + season)

    myGenericMongoClient.genericFindList('fixtures',
    {'league.id' : id_champ,
     'league.round' : 'Regular Season - '+ journee,
     "league.season" : season},
     function(err,fixtures) {
        console.log(fixtures)
        fixtures.sort(function (a,b) {

         var a1st = -1; //negative value means left item should appear first
         var b1st =  1; //positive value means right item should appear first
         var equal = 0; //zero means objects are equal
     
         //compare your object's property values and determine their order
         if (b.fixture.date < a.fixture.date) {
             return b1st;
         }
         else if (a.fixture.date < b.fixture.date) {
             return a1st;
         }
         else {
             return equal;
         }
        })

        res.send(fixtures)
     })
})




///////////////////////////////// Recherche matchs equipe dans un championnat ////////////////////////////

apiRouter.route('/fixtures/team/:id/season/:season')
.get( function(req,res,next) {

    var id = Number(req.params.id)
    var season = Number(req.params.season)
    
   var matchs = []
    myGenericMongoClient.genericFindList('fixtures',
    {'teams.away.id' : id,
     "league.season" : season},
     function(err,fixtures) {
        //console.log(fixtures)
        fixtures.forEach(element => {
           matchs.push(element)
        });
        //matchs.push(fixtures)
        //res.send(fixtures)
     })
     myGenericMongoClient.genericFindList('fixtures',
    {'teams.home.id' : id,
     "league.season" : season},
     function(err,fixtures) {
        //console.log(fixtures)
        fixtures.forEach(element => {
       matchs.push(element)
      });  
        //matchs.push(fixtures)
        var matchtrier = []
        matchs.forEach(element => {
           var array = []
           var char
           array = element.league.round
           var char1 = array.charAt(array.length-1)
           var char2 = array.charAt(array.length-2)
           if(char2==null){
              char = char1
           }
           char = char2 + char1
           //console.log(jour)
           jour = Number(char)
           matchtrier[jour-1] = element
        })    
        matchtrier.forEach(element => {
           
           //console.log(element.league.round)
        })

        res.send(matchtrier)
     })
     

})

apiRouter.route('/fixtures/38journee/:season/:id')
.get( function (req,res,next) {
   var season = Number(req.params.season)
   var id = Number(req.params.id)
   myGenericMongoClient.genericFindList('fixtures',
   { '$and': [{"league.season":season}, {"league.round":"Regular Season - 38"},{"league.id":id}]},
   function(err,fixtures) {
      res.send(fixtures)
   }
   )
})

////////////////////////////////////////// Recherche head to head  //////////////////////////

apiRouter.route('/fixtures/headtohead/teams/:team1/:team2')
.get(function (req,res,next) {
   var team1 = Number(req.params.team1)
   var team2 = Number(req.params.team2)
   myGenericMongoClient.genericFindList('fixtures',
   {'$or' : [{ '$and': [{"teams.away.id":team1}, {"teams.home.id":team2}]},{ '$and': [{"teams.home.id":team1}, {"teams.away.id":team2}]}]},
   function(err,fixtures) {
      fixtures.sort(function (a,b) {

         var a1st = -1; //negative value means left item should appear first
         var b1st =  1; //positive value means right item should appear first
         var equal = 0; //zero means objects are equal
     
         //compare your object's property values and determine their order
         if (a.fixture.date < b.fixture.date) {
             return b1st;
         }
         else if (b.fixture.date < a.fixture.date) {
             return a1st;
         }
         else {
             return equal;
         }
        })

      fixtures.splice(6,8)
      res.send(fixtures)
   })
})

////////////////////////////////////////////////// Photos des joueurs de l'equipe domicile ////////////////////////////////////////////////////////////

apiRouter.route('/fixtures/photo-home/:id')
.get(function(req,res,next) {
   var id = Number(req.params.id)
   myGenericMongoClient.genericFindOne('fixtures_spe',
    {'fixture.id' : id},
     function(err,fixtures) {

        var photos = {}
        var values = []
        var keys = []
        var players = fixtures.players[0].players
        players.forEach(element => {
           values.push(element.player.photo)
           keys.push(element.player.id)
        })

        for(var i=0; i<keys.length; i++) {
           photos[keys[i]] = values[i]
        }

        res.send(photos)
        })

        
 })

 ////////////////////////////////////////////////// Photos des joueurs de l'equipe exterieure ////////////////////////////////////////////////////////////

 apiRouter.route('/fixtures/photo-away/:id')
 .get(function(req,res,next) {
    var id = Number(req.params.id)
    myGenericMongoClient.genericFindOne('fixtures_spe',
     {'fixture.id' : id},
      function(err,fixtures) {
 
         var photos = {}
         var values = []
         var keys = []
         var players = fixtures.players[1].players
         players.forEach(element => {
            values.push(element.player.photo)
            keys.push(element.player.id)
         })
 
         for(var i=0; i<keys.length; i++) {
            photos[keys[i]] = values[i]
         }
 
         res.send(photos)
         })
 
         
  })

  /////////////////////////////////////////////////////// Fixtures selon son id //////////////////////////////////////

apiRouter.route('/fixtures/id/:id')
.get( function(req,res,next) {

    var id = Number(req.params.id)
    
    myGenericMongoClient.genericFindOne('fixtures_spe',
    {'fixture.id' : id},
     function(err,fixtures) {

      if(fixtures.lineups[0] != null) {

         fixtures.lineups[0].startXI.forEach(element => {  
            var string = element.player.grid
                          
            newString = "_"+string.slice(0, string.length-2)
            + string.slice(2, string.length);
   
            element.player.grid = newString
           })
   
           fixtures.lineups[1].startXI.forEach(element2 => {  
            var string1 = element2.player.grid
                          
            newString1 = "_"+string1.slice(0, string1.length-2)
            + string1.slice(2, string1.length);
   
            element2.player.grid = newString1
           })
      }
   
       

        res.send(fixtures)
     })
})

/////////////////////////////////////////////////////// Fixtures selon son id //////////////////////////////////////
/*
apiRouter.route('/fixtures/id/:id')
.get( function(req,res,next) {

    var id = Number(req.params.id)
    
    myGenericMongoClient.genericFindOne('fixtures_spe',
    {'fixture.id' : id},
     function(err,fixtures) {
        console.log(fixtures)
        res.send(fixtures)
     })
})
*/

/////////////////////////////////////////////////////////////// But pour l'equipe a l'exterieur //////////////////////////////////
apiRouter.route('/fixtures/goalaway/id/:id')
.get( function(req,res,next) {

    var id = Number(req.params.id)
    
    myGenericMongoClient.genericFindOne('fixtures_spe',
    {'fixture.id' : id},
     function(err,fixtures) {
        console.log(fixtures)
        var goalsaway = []
        var events = []
        events = fixtures.events
        events.forEach(element => {
           if(element.type=="Goal"&&element.team.id==fixtures.teams.away.id){
              goalsaway.push(element)
           }
        })      

        res.send(goalsaway)
        
     })
})

/////////////////////////////////////////////////////////////// But pour l'equipe a domicile //////////////////////////////////
apiRouter.route('/fixtures/goalhome/id/:id')
.get( function(req,res,next) {

    var id = Number(req.params.id)
    
    myGenericMongoClient.genericFindOne('fixtures_spe',
    {'fixture.id' : id},
     function(err,fixtures) {
        console.log(fixtures)
        var goalshome = []
        var events = []
        events = fixtures.events
        events.forEach(element => {
           if(element.type=="Goal"&&element.team.id==fixtures.teams.home.id){
              goalshome.push(element)
           }
        })      

        res.send(goalshome)
        
     })
})


exports.apiRouter = apiRouter