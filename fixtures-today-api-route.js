var express = require('express');
const apiRouter = express.Router();
require('dotenv').config()
var myGenericMongoClient = require('./my_generic_mongo_client');

var myMongoDbUrl = process.env.URL_DB
myGenericMongoClient.setMongoDbUrl(myMongoDbUrl);


apiRouter.route("/fixtures/today/:date/:id")
.get( function(req,res,next) {

    var date = req.params.date
    var id = Number(req.params.id)

    myGenericMongoClient.genericFindList('fixtures',
    {'fixture.date' : {$regex : date}, 'league.id': id},
    function(err,fixtures) {
        console.log(fixtures)
        fixtures.sort(function (a,b) {

            var a1st = -1; //negative value means left item should appear first
            var b1st =  1; //positive value means right item should appear first
            var equal = 0; //zero means objects are equal
        
            //compare your object's property values and determine their order
            if (b.fixture.date < a.fixture.date) {
                return b1st;
            }
            else if (a.fixture.date < b.fixture.date) {
                return a1st;
            }
            else {
                return equal;
            }
           })
        res.send(fixtures)
    })
})


exports.apiRouter = apiRouter