var express = require('express');
const apiRouter = express.Router();
require('dotenv').config()
var myGenericMongoClient = require('./my_generic_mongo_client');

var myMongoDbUrl = process.env.URL_DB
myGenericMongoClient.setMongoDbUrl(myMongoDbUrl);


apiRouter.route("/league/:id")
.get( function(req,res,next) {

    var id_league = Number(req.params.id)

    myGenericMongoClient.genericFindOne('league',
    {'league.id' : id_league},
     function(err,league) {
        console.log(league)
        res.send(league)
     })

})


exports.apiRouter = apiRouter