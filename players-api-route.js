var express = require('express');
const apiRouter = express.Router();
require('dotenv').config()
var myGenericMongoClient = require('./my_generic_mongo_client');
var myMongoDbUrl = process.env.URL_DB
myGenericMongoClient.setMongoDbUrl(myMongoDbUrl);

apiRouter.route("/players/:id")
.get( function(req,res,next) {

    var id = Number(req.params.id)

    myGenericMongoClient.genericFindOne('players',
    {'player.id' : id},
    function(err,player) {
        res.send(player)
    })
})


apiRouter.route('/teams/players/teamid/:id')
.get( function(req,res,next) {
    var id = Number(req.params.id);
    console.log(id);

    myGenericMongoClient.genericFindList('players',
    {'statistics.team.id' : id }, 
    function(err,players) {
        //console.log(players);

        players.sort(function (a,b) {

            var a1st = -1; //negative value means left item should appear first
            var b1st =  1; //positive value means right item should appear first
            var equal = 0; //zero means objects are equal
        
            //compare your object's property values and determine their order
            if (a.statistics[0].games.minutes < b.statistics[0].games.minutes) {
                return b1st;
            }
            else if (b.statistics[0].games.minutes < a.statistics[0].games.minutes) {
                return a1st;
            }
            else {
                return equal;
            }
           })

        res.send(players);
    })

})

/*
apiRouter.route('/teams/players/nationality/teamid/:id/:season')
.get( function(req,res,next) {
    var id = Number(req.params.id);
    var season = Number(req.params.season);
    console.log(id);

    myGenericMongoClient.genericFindList('players',
    {'statistics.team.id' : id, 'statistics.league.season': season}, 
    function(err,players) {
        var nationsPlayer = []
        var nations = []
        players.forEach(element => {
            if(nations.includes(element.player.nationality)){
                nationsPlayer[element.player.nationality] = nationsPlayer[element.player.nationality] + 1
            }
            else {
                nations.push(element.player.nationality)
                nationsPlayer[element.player.nationality] = 1
            }
            

        });
        res.send(nationsPlayer)
    })

})
*/


exports.apiRouter = apiRouter