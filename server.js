const { request, response } = require('express');
var express = require('express');
var app = express();
var classApiRoute = require('./classement-api-route');
var fixtApiRoute = require('./fixtures-api-route');
var teamApiRoute = require('./teams-api-route');
var leagueApiRoute = require('./league-api-routes')
var todayApiRoute = require('./fixtures-today-api-route')
var playerApiRoute = require('./players-api-route')
require('dotenv').config()
//support parsing of JSON post data
var jsonParser = express.json({  extended: true}); 
app.use(jsonParser);


// CORS enabled with express/node-js :
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');

  // authorized headers for preflight requests
  // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();

  app.options('*', (req, res) => {
      // allowed XHR methods  
      res.header('Access-Control-Allow-Methods', 'GET');
      res.send();
  });
});

app.use(classApiRoute.apiRouter);
app.use(fixtApiRoute.apiRouter);
app.use(teamApiRoute.apiRouter);
app.use(leagueApiRoute.apiRouter);
app.use(todayApiRoute.apiRouter);
app.use(playerApiRoute.apiRouter);
//les routes en /html/... seront gérées par express par
//de simples renvois des fichiers statiques
//du répertoire "./html"
//app.use('/html', express.static(__dirname+"/html"));
app.get('/', function(req , res ) {
  res.redirect('');
});


let backendPort = process.env.PORT || 8282; 
app.listen(backendPort , function () {
  console.log("http://localhost:"+backendPort);
});