var express = require('express');
const apiRouter = express.Router();
require('dotenv').config()
var myGenericMongoClient = require('./my_generic_mongo_client');

var myMongoDbUrl = process.env.URL_DB
console.log("myMongoDbUrl="+myMongoDbUrl)
myGenericMongoClient.setMongoDbUrl(myMongoDbUrl);


apiRouter.route('/teams/:pays')
.get( function(req,res,next) {
    var pays = req.params.pays;
    console.log(pays);

    myGenericMongoClient.genericFindList('teams',
    {'team.country' : pays }, 
    function(err,teams) {
        console.log(teams);
        res.send(teams);
    })

})


apiRouter.route('/teams/id/:id')
.get( function(req,res,next) {
    var id = Number(req.params.id);
    console.log(id);

    myGenericMongoClient.genericFindOne('teams',
    {'team.id' : id }, 
    function(err,teams) {
        console.log(teams);
        res.send(teams);
    })

})



apiRouter.route('/team/stats/:season/:id')
.get( function(req,res,next) {
    var id = Number(req.params.id)
    var season = Number(req.params.season)

    myGenericMongoClient.genericFindOne('stats',
    {'league.season':season,'team.id':id},
    function(err,stats) {
        res.send(stats)
    })
})


apiRouter.route('/team/stats/minutes/:season/:id')
.get( function(req,res,next) {
    var id = Number(req.params.id)
    var season = Number(req.params.season)
        myGenericMongoClient.genericFindOne('stats',
        {'league.season':season,'team.id':id},
        function(err,stats) {

                var stat = stats
                var minutes = ["0-15","16-30","31-45","46-60","61-75","76-90","91-105"]
                var data = []
                for(i=0;i<minutes.length;i++) {
                    data.push(stat.goals.for.minute[minutes[i]])
                }
                console.log(data)
                res.send(data)            
            });
   
    
        
    })


apiRouter.route('/team/stats/lineup/:season/:id')
.get( function(req,res,next) {
    var id = Number(req.params.id)
    var season = Number(req.params.season)
        myGenericMongoClient.genericFindOne('stats',
        {'league.season':season,'team.id':id},
        function(err,stats) {
            var lineup = stats.lineups
            var formation
            res.send(minutes)
            
            });
   
    
        
    })


exports.apiRouter = apiRouter